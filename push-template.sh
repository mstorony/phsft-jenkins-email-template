#!/bin/sh

sftp root@phsft-jenkins.cern.ch <<EOF
cd /var/lib/jenkins/email-templates
put jenkins-matrix-email-html.template-dev
EOF

echo "File uploaded"