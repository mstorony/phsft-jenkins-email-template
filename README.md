# About
This is an email-template for matrix-configured jobs. The email template is based on the [jenkins-matrix-email-html.template](https://wiki.jenkins-ci.org/display/JENKINS/Email-ext+plugin#Email-extplugin-TemplateExamples) example. The email template generates general build result information, changes overview, architecture/toolchain build result, in addition to listings of compiler warnings, errors, and results of failed tests per configuraiton.

# How to use
Make sure that [Email-ext](https://wiki.jenkins-ci.org/display/JENKINS/Email-ext+plugin) plugin is installed on the Jenkins server. 

* Use this template by moving `jenkins-matrix-email-html.template-dev` to `/var/lib/jenkins/email-templates/` on the Jenkins server, also rename the file to `jenkins-matrix-email-html.template` when being used in production. Note that if the `email-templates` folder does not exist, this folder can be created. 
* Before enabling the template for any job, make sure that the template is working on the project you want to use it on. See section Testing for how to do this.
* Go to 'Configure' for the job where the template is going to be used.
* Under 'Editable Email Notification', set the following settings:
* Project Recipient List: Emails of those who should be notified of build failures
* Content Type: HTML (text/html)
* Default Content: `${SCRIPT, template="jenkins-matrix-email-html.template"}`
* Trigger for matrix project: Trigger only the parent job
* Save the settings

# Testing
Templates can be generated inside the browser for previously executed jobs. To do this, open a job and go to 'Email Template Testing'. Specify the template file name that is located under the email-templates folder (e.g. jenkins-matrix-email-html.template-dev), the job you want to generate a report for and hit Go! 

# Notes
The two scripts pull/push-template can be used to pull/push the template for test staging between dev and prod.

# Job Customization
The template can link commit hashes to e.g. GitHub or GitLab. This can be done by specifying the URL of the git repository where the commits are lated by adding a string parameter to the job with the name `COMMIT_URL_PREFIX` and value e.g. `https://github.com/root-mirror/root/commit/`